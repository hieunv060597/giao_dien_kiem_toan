(function ($) {
    window.onload = function () {
        $(document).ready(function () {		
			drop_navbar();
			height_to_navbar();
			open_more_nav();
			scroll_btn_bar();
			progress_bar();
			click_add_title();
			pie_chart_for_home();
			pie_chart_for_home_sm();
			bar_chart_for_home();
			close_nav_mb();
        });
    };
})(jQuery);



function drop_navbar(){
    var li_has_drop = $('.nav-bar ul .has-child');
    var ul_child_drop = $('.nav-bar ul .drop-child');
    if(li_has_drop.length == 0 && ul_child_drop.length == 0){
        return 0;
    }

    else{
        ul_child_drop.after("<span class = 'btn-drop'></span>");
        var btn_drop = $('.nav-bar ul .btn-drop');
    }

    for(var i = 0; i < btn_drop.length; i++){
        btn_drop[i].onclick = function(){
			this.classList.toggle('active');
			$(this).parent().toggleClass("active-bg");
			
            for(var k = 0; k < ul_child_drop.length; k++){
                var pannel = ul_child_drop[k].nextElementSibling;
                if(pannel.classList[1] == 'active'){
					ul_child_drop[k].classList.add('show-drop');
                }
                else{
					ul_child_drop[k].classList.remove('show-drop');
                }
            }

        }
    }
}

function open_more_nav(){
	var btn_open = document.querySelectorAll("#btn-op-bar");
	if(btn_open == null){
		return 0;
	}
	else{
		var nav = document.querySelector(".nav-bar");
		var pos = document.querySelector(".bars-mn")
		for(var i = 0; i < btn_open.length; i++){
			btn_open[i].addEventListener("click", function(){
				nav.classList.toggle("active_nav");
				pos.classList.toggle("active_bar");
			})
		}
	}
}

function close_nav_mb(){
	var btn_close = document.querySelector("#close-mb-nav");

	if(btn_close == null){
		return 0;
	}

	else{
		var nav = document.querySelector(".nav-bar");
		btn_close.addEventListener("click", function(){
			nav.classList.remove("active_nav");
		})
	}
}

function height_to_navbar(){
    var height_nav = $('.nav-bar').height();
	var height_fix = $('main').height();
  
    if(height_fix >=800){
        height_nav = height_fix;
        $('.nav-bar').height(height_nav);
	}

	
	else{
		$('.nav-bar').css('height','90vh');
	}
}

function scroll_btn_bar(){
	var btn_bar = $('.bars-mn');
	if(btn_bar == null){
		return 0;
	}
	else{
		var off = btn_bar.offset().top;
		off = off / 5;
		window.addEventListener('scroll', function(){
			if(window.pageYOffset >= off){
				btn_bar.addClass('stick-fix');
			}
			else{
				btn_bar.removeClass('stick-fix');
			}
		})
	}
	
}

function progress_bar(){
	var delay = 300;
	$(".progress-bar").each(function(i) {
	$(this).delay(delay * i).animate({
		width: $(this).attr('aria-valuenow') + '%'
	}, delay);
	
	});
	
}

function click_add_title(){
	var add = document.querySelectorAll('.add-new-content-box .add-new-title');
	if(add.length == null){
		return 0;
	}

	else{
		for(var i = 0; i<add.length; i++){
			add[i].addEventListener('click', function(){
				this.classList.toggle('active_add');
			})
		}
	}
}

function pie_chart_for_home(){
	var chart_box = document.querySelector(".main-chart");
	if(chart_box == null){
		return 0;
	}

	else{
		var randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		};
		var ctx = document.getElementById("myChart");
		if(ctx == null){
			return 0;
		}
		else{
			var myChart = new Chart(ctx, {
				type: 'doughnut',
				data: {
					labels: ['Đã trình duyệt', 'Đã xét duyệt', 'Đơn vị trình phát hành', 'Yêu cầu sửa', 'Đã phát hành', 'Vụ TH trình phát hành'],
					datasets: [{
					label: '# of Tomatoes',
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
					],
					backgroundColor: [
						'#164396',
						'#0061AE',
						'#FFB100',
						'#3DB6EC',
						'#90D5E4',
						'#A3C857'
					],
					borderWidth: 1
					}]
				},
				options: {
					cutoutPercentage: 70,
					responsive: true,
					legend: {
						position: 'bottom',
						display: false,
					},
					title: {
						display: false,
						text: 'Chart.js Doughnut Chart'
					},
					animation: {
						animateScale: true,
						animateRotate: true
					},

					tooltips: {
						callbacks: {
						title: function(tooltipItem, data) {
						return data['labels'][tooltipItem[0]['index']];
						},
						label: function(tooltipItem, data) {
						return data['datasets'][0]['data'][tooltipItem['index']];
						},
						afterLabel: function(tooltipItem, data) {
						var dataset = data['datasets'][0];
						var percent = Math.round((dataset['data'][tooltipItem['index']] / dataset["_meta"][0]['total']) * 100)
						return '(' + percent + '%)';
						}
						},
						backgroundColor: '#2196F3',
						bodyFontSize: 14,
						displayColors: false,
					}
				}
			});
		}
	}
	
	
}

function pie_chart_for_home_sm(){
	var chart_box = document.querySelector(".main-chart");
	if(chart_box == null){
		return 0;
	}

	else{
		var randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		};
		var ctx = document.getElementById("myChart_two");
		if(ctx == null){
			return 0;
		}
		else{
			var myChart = new Chart(ctx, {
				type: 'doughnut',
				data: {
					labels: ['one', 'two', 'three'],
					datasets: [{
					label: '# of Tomatoes',
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
					],
					backgroundColor: [
						'#164396',
						'#00BCD5',
						'#FFA726'
					],
					borderWidth: 1
					}]
				},
				options: {
					cutoutPercentage: 70,
					responsive: true,
					legend: {
						position: 'bottom',
						display: false,
					},
					title: {
						display: false,
						text: 'Chart.js Doughnut Chart'
					},
					animation: {
						animateScale: true,
						animateRotate: true
					},
					tooltips: {
						callbacks: {
							title: function(tooltipItem_two, data) {
							return data['labels'][tooltipItem_two[0]['index']];
							},
							label: function(tooltipItem_two, data) {
							return data['datasets'][0]['data'][tooltipItem_two['index']];
							},
						},
						backgroundColor: '#2196F3',
						bodyFontSize: 14,
						displayColors: false,
					}
				}
			});
		}
	}
	
	
}

function bar_chart_for_home(){
	var chart_box = document.querySelector(".main-chart");
	
	if(chart_box == null){
		return 0;
	}

	else{
		Chart.elements.Rectangle.prototype.draw = function() {
    
			var ctx = this._chart.ctx;
			var vm = this._view;
			var left, right, top, bottom, signX, signY, borderSkipped, radius;
			var borderWidth = vm.borderWidth;
			// Set Radius Here
			// If radius is large enough to cause drawing errors a max radius is imposed
			var cornerRadius = 20;
		
			if (!vm.horizontal) {
				// bar
				left = vm.x - vm.width / 2;
				right = vm.x + vm.width / 2;
				top = vm.y;
				bottom = vm.base;
				signX = 1;
				signY = bottom > top? 1: -1;
				borderSkipped = vm.borderSkipped || 'bottom';
			} else {
				// horizontal bar
				left = vm.base;
				right = vm.x;
				top = vm.y - vm.height / 2;
				bottom = vm.y + vm.height / 2;
				signX = right > left? 1: -1;
				signY = 1;
				borderSkipped = vm.borderSkipped || 'left';
			}
		
			// Canvas doesn't allow us to stroke inside the width so we can
			// adjust the sizes to fit if we're setting a stroke on the line
			if (borderWidth) {
				// borderWidth shold be less than bar width and bar height.
				var barSize = Math.min(Math.abs(left - right), Math.abs(top - bottom));
				borderWidth = borderWidth > barSize? barSize: borderWidth;
				var halfStroke = borderWidth / 2;
				// Adjust borderWidth when bar top position is near vm.base(zero).
				var borderLeft = left + (borderSkipped !== 'left'? halfStroke * signX: 0);
				var borderRight = right + (borderSkipped !== 'right'? -halfStroke * signX: 0);
				var borderTop = top + (borderSkipped !== 'top'? halfStroke * signY: 0);
				var borderBottom = bottom + (borderSkipped !== 'bottom'? -halfStroke * signY: 0);
				// not become a vertical line?
				if (borderLeft !== borderRight) {
					top = borderTop;
					bottom = borderBottom;
				}
				// not become a horizontal line?
				if (borderTop !== borderBottom) {
					left = borderLeft;
					right = borderRight;
				}
			}
		
			ctx.beginPath();
			ctx.fillStyle = vm.backgroundColor;
			ctx.strokeStyle = vm.borderColor;
			ctx.lineWidth = borderWidth;
		
			// Corner points, from bottom-left to bottom-right clockwise
			// | 1 2 |
			// | 0 3 |
			var corners = [
				[left, bottom],
				[left, top],
				[right, top],
				[right, bottom]
			];
		
			// Find first (starting) corner with fallback to 'bottom'
			var borders = ['bottom', 'left', 'top', 'right'];
			var startCorner = borders.indexOf(borderSkipped, 0);
			if (startCorner === -1) {
				startCorner = 0;
			}
		
			function cornerAt(index) {
				return corners[(startCorner + index) % 4];
			}
		
			// Draw rectangle from 'startCorner'
			var corner = cornerAt(0);
			ctx.moveTo(corner[0], corner[1]);
		
			for (var i = 1; i < 4; i++) {
				corner = cornerAt(i);
				nextCornerId = i+1;
				if(nextCornerId == 4){
					nextCornerId = 0
				}
		
				nextCorner = cornerAt(nextCornerId);
		
				width = corners[2][0] - corners[1][0];
				height = corners[0][1] - corners[1][1];
				x = corners[1][0];
				y = corners[1][1];
				
				var radius = cornerRadius;
				
				// Fix radius being too large
				if(radius > height/2){
					radius = height/2;
				}if(radius > width/2){
					radius = width/2;
				}
		
				ctx.moveTo(x + radius, y);
				ctx.lineTo(x + width - radius, y);
				ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
				ctx.lineTo(x + width, y + height - radius);
				ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
				ctx.lineTo(x + radius, y + height);
				ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
				ctx.lineTo(x, y + radius);
				ctx.quadraticCurveTo(x, y, x + radius, y);
		
			}
		
			ctx.fill();
			if (borderWidth) {
				ctx.stroke();
			}
		}; 
		var label = ['Tăng thu NSNN', 'Giảm chi thường', 'Giảm chi đầu tư', 'Kiến nghị khác'];
		var color = Chart.helpers.color;
		var barChartData = {
			labels: ["Tăng thu NSNN", "Giảm chi thường", "Giảm chi đầu tư", "Kiến nghị khác"],
			datasets: [{
				label: "My First dataset",
				backgroundColor: "#164396",
				data: [1500, 1000, 700, 500],
				barPercentage: 1.0,
				barThickness: 17,
				

			
			}, {
				label: "My Second dataset",
				backgroundColor: "#2196F3",
				data: [1000, 1200, 800, 1700, 950],
				barPercentage: 1.0,
				barThickness: 17
			},
			{
				label: "My third dataset",
				backgroundColor: "#FFA726",
				data: [400, 300, 550, 550],
				barPercentage: 1.0,
				barThickness: 17
			}]

		};
		
	  
	var ctx = document.getElementById('myBarGraph').getContext('2d');
	if(ctx == null){
		return 0;
	}

	else{
		window.myBar = new Chart(ctx, {
			type: 'bar',
			data: barChartData,
			options: {
				barRoundness: 10,
				responsive: true,
				legend: {
					position: 'top',
					display: false,
					
				},
				title: {
					display: false,
					text: 'Chart.js Bar Chart'
				},
				scales: {
					xAxes: [
					  {
						barPercentage: 1,
						categoryPercentage: 1,
						ticks: {
							fontSize: 16
						},
						gridLines: {
						  display: false
						}
					  }
					],
					yAxes: [
					  {
						ticks: {
							fontSize: 16,
							beginAtZero:true,
						},
						gridLines: {
						  color: 'rgba(0,0,0,0.05)'
						},
					  }
					]
				}
			}
		});
	}
	}
	
	
}





